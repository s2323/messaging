module bitbucket.org/s2323/messaging

go 1.16

require (
	github.com/gdamore/tcell/v2 v2.4.1-0.20210905002822-f057f0a857a1
	github.com/posener/ctxutil v1.0.0
	github.com/rivo/tview v0.0.0-20211109175620-badfa0f0b301
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.19.1
)
