package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
	"unicode"

	"github.com/rivo/tview"

	"bitbucket.org/s2323/messaging/internal/app/client"
	"bitbucket.org/s2323/messaging/internal/pkg/commands"
	"bitbucket.org/s2323/messaging/internal/pkg/model"
)

type messageContext struct {
	msg *model.Message
}

func completer(currentText string) (entries []string) {
	if len(currentText) == 1 && currentText[0] == '/' {
		return []string{"/peer", "/broadcast", model.ServerCommandList.String()}
	}
	if len(currentText) >= 1 && currentText[0] == '/' {
		if strings.HasPrefix("/peer", currentText) {
			return []string{"/peer"}
		}
		if strings.HasPrefix("/broadcast", currentText) {
			return []string{"/broadcast"}
		}
		if strings.HasPrefix(model.ServerCommandList.String(), currentText) {
			return []string{model.ServerCommandList.String()}
		}
	}
	return nil
}

func (m *messageContext) setPrompt(input *tview.InputField) {
	if m.msg != nil {
		switch m.msg.Code {
		case model.MessageCodeBroadcast:
			input.SetLabel("[!!! broadcast !!!] >>> ")
			return
		case model.MessageCodeSendToPeer:
			input.SetLabel(fmt.Sprintf("[%d] >>> ", m.msg.Peer))
			return
		}
	}
	input.SetLabel("[set peer with /peer or /broadcast] >>> ")
}

func (m *messageContext) historyLog(textView *tview.TextView, in string) {
	if m.msg == nil || len(in) == 0 {
		return
	}
	if m.msg.Code == model.MessageCodeServerCmd {
		return
	}

	ts := time.Now().Format(time.RFC3339)
	in = tview.Escape(in)
	switch m.msg.Code {
	case model.MessageCodeBroadcast:
		_, _ = fmt.Fprintf(textView, "[::b][%s][!!! broadcast !!!]   >>> %s[::-]\n", ts, in)
	case model.MessageCodeSendToPeer:
		_, _ = fmt.Fprintf(textView, "[::b][%s][%d] >>> %s[::-]\n", ts, m.msg.Peer, in)
	default:
		_, _ = fmt.Fprintf(textView, "[::b][%s][server] >>> %s[::-]\n", ts, in)
	}
}

func (m *messageContext) exec(c *client.Client, in string) error {
	command, args := model.ParseServerCommand(in)
	switch command {
	case model.ServerCommandList:
		err := m.cmdList(args)
		if err != nil {
			return err
		}

	case model.ServerCommandNULL:
		if len(args) == 0 {
			return nil
		}

		switch strings.ToLower(args[0]) {
		case "/peer":
			err := m.cmdPeer(in, args)
			if err != nil {
				return err
			}

		case "/broadcast":
			err := m.cmdBroadcast(in, args)
			if err != nil {
				return err
			}

		default:
			if m.msg != nil {
				m.msg.WithPayload([]byte(in))
				break
			}
		}
	}

	if m.msg != nil && len(m.msg.Raw) > 0 {
		c.SendMessage(m.msg)
		switch m.msg.Code {
		case model.MessageCodeSendToPeer:
			m.msg = model.NewMessage(m.msg.Peer)
		case model.MessageCodeBroadcast:
			m.msg = model.NewBroadcastMessage()
		default:
			m.msg = &model.Message{}
		}
	}
	return nil
}

func (m *messageContext) cmdPeer(_ string, fields []string) error {
	if len(fields) != 2 {
		return errors.New("usage: /peer <peer>")
	}

	peer, err := strconv.Atoi(fields[1])
	if err != nil {
		return err
	}
	if peer <= 0 {
		m.msg = model.NewBroadcastMessage()
	} else {
		m.msg = model.NewMessage(model.Tag(peer))
	}
	return nil
}

func (m *messageContext) cmdBroadcast(in string, fields []string) error {
	m.msg = model.NewBroadcastMessage()
	if len(fields) > 1 {
		data := strings.TrimLeftFunc(in, unicode.IsSpace)
		data = strings.TrimPrefix(data, fields[0])
		data = strings.TrimLeftFunc(data, unicode.IsSpace)
		m.msg.WithPayload([]byte(data))
	}
	return nil
}

func (m *messageContext) cmdList(fields []string) error {
	if len(fields) != 2 {
		return errors.New("usage: /list <page> <page size>")
	}
	m.msg = model.NewServerCommand()
	cmd := &commands.ListPeers{}

	page, err := strconv.Atoi(fields[0])
	if err != nil {
		return err
	}
	cmd.Page = page

	pageSize, err := strconv.Atoi(fields[1])
	if err != nil {
		return err
	}
	cmd.PageSize = pageSize

	err = commands.WriteToMessage(m.msg, cmd)
	return err
}
