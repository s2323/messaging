package main

import (
	"flag"
	"fmt"
	"io"
	"time"

	"github.com/gdamore/tcell/v2"
	"github.com/posener/ctxutil"
	"github.com/rivo/tview"

	"bitbucket.org/s2323/messaging/internal/app/client"
	"bitbucket.org/s2323/messaging/internal/config"
)

const usage = `Use commands:
	/list       - to list peers connected to the server
	/peer <Tag> - to set peer to send direct messages (use Tag = 0 to broadcast)
	/broadcast  - to enable broadcast messaging mode


`

func main() {
	addr := flag.String("addr", config.Addr, "server address and port")
	flag.Parse()

	app := tview.NewApplication()
	textView := tview.NewTextView().SetText(usage).SetDynamicColors(true)
	input := tview.NewInputField().SetAutocompleteFunc(completer)

	textView.SetTitle("[ Messages History ]").SetBorder(true)
	input.SetTitle("[ Send Message ]").SetBorder(true)
	flex := tview.NewFlex().SetDirection(tview.FlexRow).
		AddItem(textView, 0, 4, false).
		AddItem(input, 0, 1, false)

	c := &client.Client{}
	if addr != nil {
		c.Addr = *addr
	}
	ctx := ctxutil.Interrupt()
	if err := c.Run(ctx); err != nil {
		panic(err)
	}
	msgContext := &messageContext{}

	// Read
	msgContext.setPrompt(input)
	input.SetDoneFunc(func(key tcell.Key) {
		if key != tcell.KeyEnter {
			return
		}

		in := input.GetText()
		go func() {
			err := msgContext.exec(c, in)
			app.QueueUpdateDraw(func() {
				msgContext.setPrompt(input)
				input.SetText("")
				if err != nil {
					_, _ = fmt.Fprintf(textView, "err: %+v\n", err)
				} else {
					msgContext.historyLog(textView, in)
				}
			})
		}()
	})

	// Write
	go func() {
		for {
			msg, err := c.ReadMessage()
			switch {
			case msg == nil && err == nil:
				continue
			case err != nil:
				if err == io.EOF {
					break
				}
				app.QueueUpdateDraw(func() {
					_, _ = fmt.Fprintf(textView, "err: %+v\n", err)
				})
			case msg != nil:
				app.QueueUpdateDraw(func() {
					ts := time.Now().Format(time.RFC3339)
					raw := string(msg.Raw)
					if msg.Peer == 0 {
						_, _ = fmt.Fprintf(textView, "[%s][server] <<< %s\n", ts, tview.Escape(raw))
					} else {
						_, _ = fmt.Fprintf(textView, "[%s][%d] <<< %s\n", ts, msg.Peer, tview.Escape(raw))
					}
				})
			}
		}
	}()

	if err := app.SetRoot(flex, true).SetFocus(input).Run(); err != nil {
		panic(err)
	}
}
