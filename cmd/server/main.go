package main

import (
	"flag"

	"github.com/posener/ctxutil"

	"bitbucket.org/s2323/messaging/internal/app/server"
	"bitbucket.org/s2323/messaging/internal/config"
)

func main() {
	addr := flag.String("addr", config.Addr, "address and port to bind")
	flag.Parse()

	ctx := ctxutil.Interrupt()
	srv := &server.Server{}
	if addr != nil {
		srv.Addr = *addr
	}

	err := srv.Run(ctx)
	if err != nil {
		panic(err)
	}
}
