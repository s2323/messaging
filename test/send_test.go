package test

import (
	"bytes"
	"fmt"
	"math/rand"
	"sort"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"

	"bitbucket.org/s2323/messaging/internal/pkg/model"
)

const (
	messageSize = 1024 * 1024
)

var (
	sent     = make(map[string]*model.Message, (clientCount-1)*clientCount)
	received = make(map[string]*model.Message, (clientCount-1)*clientCount)

	sentBroadcast     = make([]*model.Message, 0, clientCount)
	receivedBroadcast = make(map[int][]*model.Message, clientCount)

	m = sync.Mutex{}
)

func TestSendMessages(t *testing.T) {
	t.Parallel()

	wg := &sync.WaitGroup{}
	for i := range runtimeContext.c {
		go receive(wg, i, t)
		for j := range runtimeContext.c {
			if j == i {
				continue
			}
			t.Run(fmt.Sprintf("should be able to send direct messages %d %d", i, j), sendMessages(wg, i, j))
		}
	}
	for i := range runtimeContext.c {
		t.Run(fmt.Sprintf("should be able to send broadcast messages %d", i), broadcastMessage(wg, i))
	}
	wg.Wait()

	require.Len(t, received, len(sent))
	for k, exp := range sent {
		act, ok := received[k]
		require.True(t, ok)
		require.Equal(t, exp.Code, act.Code)
		require.True(t, bytes.Equal(exp.Raw, act.Raw))
	}

	sort.Slice(sentBroadcast, func(i, j int) bool {
		return sentBroadcast[i].Peer < sentBroadcast[j].Peer
	})
	for _, act := range receivedBroadcast {
		sort.Slice(act, func(i, j int) bool {
			return act[i].Peer < act[j].Peer
		})
		for i, exp := range sentBroadcast {
			require.Equal(t, exp.Code, act[i].Code)
			require.True(t, bytes.Equal(exp.Raw, act[i].Raw))
		}
	}
}

func sendMessages(wg *sync.WaitGroup, sender, peer int) func(*testing.T) {
	wg.Add(1)
	return func(t *testing.T) {
		defer wg.Done()
		b := getRandomBytes(t, messageSize)

		msg := model.NewMessage(runtimeContext.tags[peer]).WithPayload(b)
		runtimeContext.c[sender].SendMessage(msg)

		m.Lock()
		key := fmt.Sprintf("%d:%d", runtimeContext.tags[sender], runtimeContext.tags[peer])
		sent[key] = msg
		m.Unlock()
	}
}

func broadcastMessage(wg *sync.WaitGroup, sender int) func(*testing.T) {
	wg.Add(1)
	return func(t *testing.T) {
		defer wg.Done()
		b := getRandomBytes(t, messageSize)

		msg := model.NewBroadcastMessage().WithPayload(b)
		runtimeContext.c[sender].SendMessage(msg)

		m.Lock()
		sentBroadcast = append(sentBroadcast, msg)
		m.Unlock()
	}
}

func receive(wg *sync.WaitGroup, i int, t *testing.T) {
	wg.Add(1)
	defer wg.Done()
	for j := 0; j < 2*len(runtimeContext.c)-1; {
		msg, err := runtimeContext.c[i].ReadMessage()
		require.NoError(t, err)

		if msg == nil {
			continue
		}
		if msg.Code == model.MessageCodeSendToPeer {
			m.Lock()
			key := fmt.Sprintf("%d:%d", msg.Peer, runtimeContext.tags[i])
			received[key] = msg
			m.Unlock()
		}
		if msg.Code == model.MessageCodeBroadcast {
			m.Lock()
			slice := receivedBroadcast[i]
			slice = append(slice, msg)
			receivedBroadcast[i] = slice
			m.Unlock()
		}
		j++
	}
}

func getRandomBytes(t *testing.T, size int) []byte {
	b := make([]byte, size)
	_, err := rand.Read(b)
	require.NoError(t, err)
	return b
}
