package test

import (
	"context"
	"fmt"
	"os"
	"testing"

	"github.com/posener/ctxutil"

	"bitbucket.org/s2323/messaging/internal/app/client"
	"bitbucket.org/s2323/messaging/internal/app/server"
	"bitbucket.org/s2323/messaging/internal/pkg/model"
)

const clientCount = 10

type RuntimeContext struct {
	ctx  context.Context
	s    *server.Server
	c    []*client.Client
	tags []model.Tag
}

var runtimeContext = RuntimeContext{
	ctx: ctxutil.Interrupt(),
}

func TestMain(m *testing.M) {
	os.Exit(testMain(m))
}

func testMain(m *testing.M) int {
	runtimeContext.s = &server.Server{}
	go func() {
		err := runtimeContext.s.Run(runtimeContext.ctx)
		if err != nil {
			panic(err)
		}
	}()

	waitServerConnection()
	fmt.Println("Server started")
	for i := 0; i < clientCount; i++ {
		c := &client.Client{}
		err := c.Run(runtimeContext.ctx)
		if err != nil {
			panic(err)
		}

		msg, err := c.ReadMessage()
		if err != nil {
			panic(err)
		}

		var tag int
		_, err = fmt.Sscanf(string(msg.Raw), "Your Tag: %d\n", &tag)
		if err != nil {
			panic(err)
		}

		runtimeContext.c = append(runtimeContext.c, c)
		runtimeContext.tags = append(runtimeContext.tags, model.Tag(tag))
	}
	defer func() {
		for _, c := range runtimeContext.c {
			c.Close()
		}
	}()

	return m.Run()
}

func waitServerConnection() {
	c := &client.Client{}
	for runtimeContext.ctx.Err() == nil {
		err := c.Run(runtimeContext.ctx)
		if err == nil {
			c.Close()
			return
		}
	}
}
