export GO111MODULE=on
export GOSUMDB=off

BUILD_ENVPARMS:=CGO_ENABLED=0
LOCAL_BIN:=$(CURDIR)/bin
LDFLAGS:=

.PHONY: all
all: server client test

.PHONY: build
build: .go_mod server client

.PHONY: server
server:
	$(info #Building server...)
	$(BUILD_ENVPARMS) go build -ldflags "$(LDFLAGS)" -o $(LOCAL_BIN)/server ./cmd/server/...

.PHONY: client
client:
	$(info #Building client...)
	$(BUILD_ENVPARMS) go build -ldflags "$(LDFLAGS)" -o $(LOCAL_BIN)/client ./cmd/client/...

.PHONY: test
test: .go_mod
	go test -p=1 -count=1 -v ./...

.PHONY: .go_mod
.go_mod:
	go mod tidy -v
