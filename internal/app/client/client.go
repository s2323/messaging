package client

import (
	"context"
	"io"
	"net"
	"time"

	"bitbucket.org/s2323/messaging/internal/config"
	"bitbucket.org/s2323/messaging/internal/pkg"
	"bitbucket.org/s2323/messaging/internal/pkg/commands"
	"bitbucket.org/s2323/messaging/internal/pkg/model"
)

// Client ...
type Client struct {
	conn *pkg.Connection
	Addr string
}

// Run client
func (c *Client) Run(ctx context.Context) error {
	if len(c.Addr) == 0 {
		c.Addr = config.Addr
	}

	conn, err := net.Dial("tcp", c.Addr)
	if err != nil {
		return err
	}
	c.conn = pkg.NewConnection(ctx, conn)

	go c.conn.SendMessages()
	go c.conn.RecvMessages()
	go c.heartbeat(ctx)

	return nil
}

// Close connection to the server
func (c *Client) Close() error {
	return c.conn.Close()
}

// SendMessage sends messages to the server
func (c *Client) SendMessage(msg *model.Message) {
	c.conn.Out() <- msg
}

// ReadMessage reads messages from the server
func (c *Client) ReadMessage() (*model.Message, error) {
	select {
	case msg, ok := <-c.conn.In():
		if !ok {
			return nil, io.EOF
		}
		if msg.Code == model.MessageCodeServerCmd {
			cmd, err := commands.ReadFromMessage(msg)
			if err != nil {
				return nil, err
			}
			if cmd.ServerCommand() == model.ServerCommandHeartbeat {
				heartbeat := cmd.(*commands.Heartbeat)
				heartbeat.T = append(heartbeat.T, time.Now().UnixNano())
				_ = commands.WriteToMessage(msg, heartbeat)
				c.SendMessage(msg)
				return nil, nil
			}
		}
		return msg, nil

	case err, ok := <-c.conn.Errs():
		if !ok {
			return nil, io.EOF
		}
		return nil, err
	}
}

func (c *Client) heartbeat(ctx context.Context) {
	t := time.After(10 * time.Second)
	for {
		select {
		case <-t:
			t = time.After(time.Second * 60)
			msg := model.NewServerCommand()
			_ = commands.WriteToMessage(msg, &commands.Heartbeat{
				T: []int64{time.Now().UnixNano()},
			})
			c.SendMessage(msg)

		case <-ctx.Done():
			return
		}
	}
}
