package server

import (
	"context"
	"net"

	"go.uber.org/zap"

	"bitbucket.org/s2323/messaging/internal/config"
	"bitbucket.org/s2323/messaging/internal/pkg/router"
)

// Server ...
type Server struct {
	listener  net.Listener
	msgRouter *router.MessageRouter
	logger    *zap.Logger
	shutdown  bool
	Addr      string
}

// Run server
func (s *Server) Run(ctx context.Context) error {
	if len(s.Addr) == 0 {
		s.Addr = config.Addr
	}

	listener, err := net.Listen("tcp", s.Addr)
	if err != nil {
		return err
	}
	defer listener.Close()

	s.listener = listener
	s.logger, err = zap.NewProduction()
	if err != nil {
		return err
	}
	s.msgRouter = router.NewMessageRouter(s.logger)
	return s.serve(ctx)
}

func (s *Server) serve(ctx context.Context) error {
	go func() {
		select {
		case <-ctx.Done():
			s.shutdown = true
			s.listener.Close()
		}
	}()
	for ctx.Err() == nil {
		conn, err := s.listener.Accept()
		if err != nil {
			if s.shutdown {
				err = nil
			}
			return err
		}

		err = s.msgRouter.AddConnection(ctx, conn)
		if err != nil {
			conn.Close()
		}
	}
	return nil
}
