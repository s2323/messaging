package model

import (
	"bytes"
	"encoding/binary"
	"io"
)

// MessageCode is coded command
type MessageCode uint8

// MessageCode values
const (
	_ MessageCode = iota
	MessageCodeServerCmd
	MessageCodeSendToPeer
	MessageCodeBroadcast
	MessageCodeError
)

// Message raw bytes
type Message struct {
	Code MessageCode
	Peer Tag
	Raw  []byte
}

// NewMessage creates new direct message
func NewMessage(peer Tag) *Message {
	return &Message{
		Code: MessageCodeSendToPeer,
		Peer: peer,
	}
}

// NewBroadcastMessage creates new broadcast message
func NewBroadcastMessage() *Message {
	return &Message{
		Code: MessageCodeBroadcast,
	}
}

// NewServerCommand creates new server request
func NewServerCommand() *Message {
	return &Message{
		Code: MessageCodeServerCmd,
	}
}

// WithPayload adds content raw bytes to message
func (m *Message) WithPayload(payload []byte) *Message {
	m.Raw = payload
	return m
}

const (
	maxMessageSize = 65535
	headerSize     = 9 // 1 byte for code + 8 bytes for peer
	maxChunkSize   = maxMessageSize - headerSize
)

// WriteTo writes message to w. Implements io.WriterTo
func (m *Message) WriteTo(w io.Writer) (n int64, err error) {
	msg := m.Raw
	for {
		chunk := msg
		if len(chunk) == 0 {
			break
		}
		if len(chunk) > maxChunkSize {
			chunk = chunk[0:maxChunkSize]
		}
		msg = msg[len(chunk):]

		// Write content-length
		err = binary.Write(w, binary.LittleEndian, uint16(len(chunk)+headerSize))
		if err != nil {
			return
		}
		n += 2

		// Write message code
		err = binary.Write(w, binary.LittleEndian, uint8(m.Code))
		if err != nil {
			return
		}
		n += 1

		// Write message peer
		err = binary.Write(w, binary.LittleEndian, uint64(m.Peer))
		if err != nil {
			return
		}
		n += 8

		// Write chunk
		nn, err1 := w.Write(chunk)
		if err1 != nil {
			err = err1
			return
		}
		n += int64(nn)
	}

	// Write empty chunk - End Of Message
	if n > 0 {
		err = binary.Write(w, binary.LittleEndian, uint16(0))
		if err != nil {
			return
		}
		n += 2
	}
	return
}

// ReadFrom reads message from r. Implements io.ReaderFrom
func (m *Message) ReadFrom(r io.Reader) (n int64, err error) {
	tmp := bytes.NewBuffer(make([]byte, 0, maxMessageSize))
	for {
		// Read content-length
		var size uint16
		err = binary.Read(r, binary.LittleEndian, &size)
		if err != nil {
			return
		}
		n += 2

		if size == 0 {
			break
		}

		// Read all
		nn, err1 := io.CopyN(tmp, r, int64(size))
		if err1 != nil {
			err = err1
			return
		}
		n += nn

		// Get message code
		var code uint8
		err = binary.Read(tmp, binary.LittleEndian, &code)
		if err != nil {
			return
		}
		m.Code = MessageCode(code)

		// Get message peer
		var peer uint64
		err = binary.Read(tmp, binary.LittleEndian, &peer)
		if err != nil {
			return
		}
		m.Peer = Tag(peer)

		// Get chunk
		if m.Raw == nil {
			m.Raw = make([]byte, 0, tmp.Len())
		}
		m.Raw = append(m.Raw, tmp.Bytes()...)
		tmp.Reset()
	}
	return
}
