package model

import (
	"bytes"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMessage(t *testing.T) {
	var messages []*Message
	var buffer bytes.Buffer

	t.Run("should be able to write message", func(t *testing.T) {
		msg := NewMessage(Tag(1))
		payload := "#1: test message"
		messages = append(messages, msg.WithPayload([]byte(payload)))

		_, err := msg.WriteTo(&buffer)

		require.NoError(t, err)
	})

	t.Run("should be able to write broadcast message", func(t *testing.T) {
		msg := NewBroadcastMessage()
		payload := "#2: test broadcast message"
		messages = append(messages, msg.WithPayload([]byte(payload)))

		_, err := msg.WriteTo(&buffer)

		require.NoError(t, err)
	})

	t.Run("should be able to write server command message", func(t *testing.T) {
		msg := NewServerCommand()
		payload := "#3: test server command"
		messages = append(messages, msg.WithPayload([]byte(payload)))

		_, err := msg.WriteTo(&buffer)

		require.NoError(t, err)
	})

	t.Run("should be able to write error message", func(t *testing.T) {
		msg := &Message{Code: MessageCodeError}
		payload := "#4: test error message"
		messages = append(messages, msg.WithPayload([]byte(payload)))

		_, err := msg.WriteTo(&buffer)

		require.NoError(t, err)
	})

	t.Run("should write huge message", func(t *testing.T) {
		msg := &Message{}
		payload := make([]byte, maxChunkSize*10)
		_, err := rand.Read(payload)
		require.NoError(t, err)
		messages = append(messages, msg.WithPayload(payload))

		_, err = msg.WriteTo(&buffer)

		require.NoError(t, err)
	})

	t.Run("should skip empty messages", func(t *testing.T) {
		msg := &Message{}
		size := buffer.Len()

		n, err := msg.WriteTo(&buffer)

		require.NoError(t, err)
		require.Zero(t, n)
		require.Equal(t, size, buffer.Len())
	})

	t.Run("should be able to read messages", func(t *testing.T) {
		for i := range messages {
			msg := &Message{}

			_, err := msg.ReadFrom(&buffer)

			require.NoError(t, err)
			require.Equal(t, messages[i], msg)
		}
	})
}

func Benchmark(b *testing.B) {
	messages := make([]*Message, 0, 1000)
	for i := 0; i < cap(messages); i++ {
		message := NewMessage(Tag(i))
		payload := make([]byte, 4096)

		_, err := rand.Read(payload)
		require.NoError(b, err)

		messages = append(messages, message.WithPayload(payload))
	}
	var buffer bytes.Buffer

	b.Run("write message", func(b *testing.B) {
		b.ResetTimer()
		b.ReportAllocs()
		for i := 0; i < b.N; i++ {
			msg := messages[i%len(messages)]
			n, err := msg.WriteTo(&buffer)
			require.NoError(b, err)
			b.SetBytes(n)
		}
	})

	msg := &Message{}
	b.Run("read message", func(b *testing.B) {
		b.ResetTimer()
		b.ReportAllocs()
		for i := 0; i < b.N; i++ {
			msg.Raw = msg.Raw[0:0]
			n, err := msg.ReadFrom(&buffer)
			require.NoError(b, err)
			//require.Equal(b, messages[i%len(messages)], msg)
			b.SetBytes(n)
		}
	})
}
