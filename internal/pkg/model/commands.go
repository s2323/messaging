package model

import "strings"

// ServerCommand is coded server command
type ServerCommand uint8

// ServerCommand values
const (
	ServerCommandNULL ServerCommand = iota
	ServerCommandList
	ServerCommandHeartbeat
)

// String returns string representation of ServerCommand
func (c ServerCommand) String() string {
	switch c {
	case ServerCommandList:
		return "/list"
	case ServerCommandHeartbeat:
		return "/heartbeat"
	default:
		return "<unknown server command>"
	}
}

// ParseServerCommand parses ServerCommand and its params from string
func ParseServerCommand(cmd string) (ServerCommand, []string) {
	fields := strings.Fields(cmd)
	if len(fields) == 0 {
		return ServerCommandNULL, nil
	}

	switch fields[0] {
	case ServerCommandList.String():
		return ServerCommandList, fields[1:]
	case ServerCommandHeartbeat.String():
		return ServerCommandHeartbeat, nil
	default:
		return ServerCommandNULL, fields
	}
}
