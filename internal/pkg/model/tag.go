package model

import "time"

// Tag for client identification
type Tag int

// NewTag returns new Tag
func NewTag() Tag {
	return Tag(time.Now().UnixNano())
}

// Next increments Tag value by 1
func (t *Tag) Next() {
	*t++
}
