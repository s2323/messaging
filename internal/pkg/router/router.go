package router

import (
	"context"
	"fmt"
	"net"
	"sort"
	"sync"

	"go.uber.org/zap"

	"bitbucket.org/s2323/messaging/internal/pkg/model"
)

// MessageRouter routes direct messages between peers, broadcasts messages
type MessageRouter struct {
	peers         map[model.Tag]*ClientConnection
	broadcastChan chan *sync.WaitGroup
	logger        *zap.Logger
	m             sync.RWMutex
}

// NewMessageRouter creates new MessageRouter
func NewMessageRouter(l *zap.Logger) *MessageRouter {
	broadcastChan := make(chan *sync.WaitGroup, 8)
	for i := 0; i < cap(broadcastChan); i++ {
		broadcastChan <- &sync.WaitGroup{}
	}
	return &MessageRouter{
		peers:         make(map[model.Tag]*ClientConnection, 10000),
		broadcastChan: broadcastChan,
		logger:        l,
	}
}

// AddConnection adds new connected client into messaging
func (r *MessageRouter) AddConnection(ctx context.Context, conn net.Conn) error {
	tag, err := r.getTag(ctx)
	if err != nil {
		r.logger.Error("connection get tag", zap.Error(err))
		return err
	}

	clientConn := NewClientConnection(ctx, conn, tag)
	err = r.registerClientConnection(clientConn)
	if err != nil {
		r.logger.Error("connection register", zap.Error(err))
		return err
	}
	r.logger.Info("new client connected", zap.Int("tag", int(tag)))

	go r.processClient(clientConn)

	return nil
}

func (r *MessageRouter) getTag(ctx context.Context) (model.Tag, error) {
	tag := model.NewTag()
	for {
		_, ok := r.getPeer(tag)
		if !ok {
			break
		}
		if ctx.Err() != nil {
			return tag, ctx.Err()
		}
		tag.Next()
	}
	return tag, nil
}

func (r *MessageRouter) registerClientConnection(conn *ClientConnection) error {
	r.m.Lock()
	defer r.m.Unlock()

	_, ok := r.peers[conn.Tag]
	if ok {
		return conn.Close()
	}
	r.peers[conn.Tag] = conn
	return nil
}

func (r *MessageRouter) processClient(conn *ClientConnection) {
	msg := model.NewMessage(conn.Tag).WithPayload([]byte(
		fmt.Sprintf("Your Tag: %d\n", conn.Tag),
	))
	conn.Write(msg)

	for {
		msg = conn.Read()
		if msg == nil {
			_ = conn.Close()

			r.m.Lock()
			delete(r.peers, conn.Tag)
			r.m.Unlock()

			return
		}

		switch msg.Code {
		case model.MessageCodeBroadcast:
			r.logger.Info("broadcast message", zap.Int("sender", int(conn.Tag)))
			r.broadcast(msg, conn)
		case model.MessageCodeSendToPeer:
			r.logger.Info("route message", zap.Int("peer", int(msg.Peer)), zap.Int("sender", int(conn.Tag)))
			r.route(msg, conn)
		case model.MessageCodeServerCmd:
			r.logger.Info("server command", zap.Int("sender", int(conn.Tag)))
			r.exec(msg, conn)
		}
	}
}

func (r *MessageRouter) broadcast(msg *model.Message, sender *ClientConnection) {
	wg := <-r.broadcastChan
	{
		peers := r.getPeers()
		tags := make([]model.Tag, 0, len(peers))
		for tag := range peers {
			tags = append(tags, tag)
		}
		sort.Slice(tags, func(i, j int) bool {
			return peers[tags[j]].latency < peers[tags[i]].latency // ORDER BY latency DESC
		})

		msg.Peer = sender.Tag
		for _, tag := range tags {
			wg.Add(1)
			conn := peers[tag]
			conn.WriteNotify(msg, wg)
		}
		wg.Wait()
	}
	r.broadcastChan <- wg
}

func (r *MessageRouter) route(msg *model.Message, sender *ClientConnection) {
	conn, ok := r.getPeer(msg.Peer)
	if !ok {
		err := fmt.Errorf("unknown client with Tag: %d", msg.Peer)
		sender.Error(err)
		return
	}

	msg.Peer = sender.Tag
	conn.Write(msg)
}

func (r *MessageRouter) getPeers() map[model.Tag]*ClientConnection {
	r.m.RLock()
	peers := r.peers
	r.m.RUnlock()
	return peers
}

func (r *MessageRouter) getPeer(tag model.Tag) (*ClientConnection, bool) {
	peers := r.getPeers()
	peer, ok := peers[tag]
	return peer, ok
}
