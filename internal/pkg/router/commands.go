package router

import (
	"bytes"
	"fmt"
	"sort"
	"time"

	"bitbucket.org/s2323/messaging/internal/pkg/commands"
	"bitbucket.org/s2323/messaging/internal/pkg/model"
)

// ServerCommand interface
type ServerCommand interface {
	Apply(r *MessageRouter, conn *ClientConnection) *model.Message
}

func (r *MessageRouter) exec(msg *model.Message, sender *ClientConnection) {
	cmd, err := commands.ReadFromMessage(msg)
	if err != nil {
		sender.Error(err)
		return
	}

	var command ServerCommand
	switch cmd.ServerCommand() {
	case model.ServerCommandList:
		command = &ListPeers{ListPeers: cmd.(*commands.ListPeers)}
	case model.ServerCommandHeartbeat:
		command = &Heartbeat{Heartbeat: cmd.(*commands.Heartbeat)}
	default:
		err = fmt.Errorf("unsupported server command: code = %d (%s)", cmd.ServerCommand(), cmd.ServerCommand().String())
		sender.Error(err)
		return
	}

	msg = command.Apply(r, sender)
	if msg != nil {
		sender.Write(msg)
	}
}

// ListPeers is server command to paginate over connected peers
type ListPeers struct {
	*commands.ListPeers
}

// Apply implements ServerCommand interface. Returns message with connected peers page
func (c *ListPeers) Apply(r *MessageRouter, conn *ClientConnection) *model.Message {
	peers := r.getPeers()
	tags := make([]model.Tag, 0, len(peers))
	for tag := range peers {
		tags = append(tags, tag)
	}
	sort.Slice(tags, func(i, j int) bool {
		return tags[i] < tags[j]
	})

	from := (c.Page - 1) * c.PageSize
	if from > len(tags) {
		from = len(tags)
	}
	tags = tags[from:]
	if len(tags) > c.PageSize {
		tags = tags[0:c.PageSize]
	}

	var sb bytes.Buffer
	sb.WriteString("Connected peers:\n")
	for _, tag := range tags {
		sb.WriteString(fmt.Sprintf("\t%d", tag))
		if tag == conn.Tag {
			sb.WriteByte('\t')
			sb.WriteString("*SELF*")
		}
		sb.WriteByte('\n')
	}
	return model.NewMessage(0).WithPayload(sb.Bytes())
}

// Heartbeat is server command to hold keep-alive connection and calculate peer's latency
type Heartbeat struct {
	*commands.Heartbeat
}

// Apply implements ServerCommand interface. Sets connection latency
func (c *Heartbeat) Apply(_ *MessageRouter, conn *ClientConnection) *model.Message {
	if len(c.T) < 3 {
		c.T = append(c.T, time.Now().UnixNano())

		msg := model.NewServerCommand()
		_ = commands.WriteToMessage(msg, c)
		return msg
	}

	now := time.Now().UnixNano()
	clientLatency := time.Duration(c.T[2]-c.T[0]) / 2
	serverLatency := time.Duration(now-c.T[1]) / 2
	conn.latency = (clientLatency + serverLatency) / 2

	bias1 := time.Duration(c.T[1] - c.T[0])
	bias2 := time.Duration(c.T[2] - c.T[1])
	bias3 := time.Duration(now - c.T[2])
	conn.bias = (bias1+bias2+bias3)/3 - conn.latency

	//fmt.Printf("Latency>> %s; Bias>> %s\n", conn.latency.String(), conn.bias.String())
	return nil
}
