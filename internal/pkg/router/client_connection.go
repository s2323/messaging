package router

import (
	"context"
	"net"
	"sync"
	"time"

	"bitbucket.org/s2323/messaging/internal/pkg"
	"bitbucket.org/s2323/messaging/internal/pkg/model"
)

// ClientConnection wrapper to handle client's TCP connection
type ClientConnection struct {
	conn *pkg.Connection
	Tag  model.Tag

	// metrics
	latency time.Duration
	bias    time.Duration
}

// NewClientConnection wraps TCP connection into new ClientConnection. Specified context.Context used to stop any goroutines
func NewClientConnection(ctx context.Context, conn net.Conn, tag model.Tag) *ClientConnection {
	c := &ClientConnection{
		conn: pkg.NewConnection(ctx, conn),
		Tag:  tag,
	}

	go c.conn.SendMessages()
	go c.conn.RecvMessages()

	return c
}

// Close connection
func (c *ClientConnection) Close() error {
	return c.conn.Close()
}

// Error puts error into outgoing queue
func (c *ClientConnection) Error(err error) {
	msg := (&model.Message{Code: model.MessageCodeError}).WithPayload([]byte(err.Error()))
	c.Write(msg)
}

// Write puts message into outgoing queue
func (c *ClientConnection) Write(msg *model.Message) {
	select {
	case c.conn.Out() <- msg:
	default:
		go func() {
			c.conn.Out() <- msg
		}()
	}
}

// WriteNotify puts message into outgoing queue, notifies on success by wg.Done()
func (c *ClientConnection) WriteNotify(msg *model.Message, wg *sync.WaitGroup) {
	select {
	case c.conn.Out() <- msg:
		wg.Done()
	default:
		go func() {
			defer wg.Done()
			c.conn.Out() <- msg
		}()
	}
}

// Read gets message from incoming queue
func (c *ClientConnection) Read() *model.Message {
	select {
	case msg, ok := <-c.conn.In():
		if !ok {
			return nil
		}
		return msg
	case err, ok := <-c.conn.Errs():
		if !ok {
			return nil
		}
		c.Error(err)
		return nil
	}
}
