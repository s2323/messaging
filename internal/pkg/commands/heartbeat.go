package commands

import (
	"encoding/binary"
	"io"

	"bitbucket.org/s2323/messaging/internal/pkg/model"
)

// Heartbeat for keep-alive and latency calculation
type Heartbeat struct {
	T []int64
}

// ReadParams reads timestamp and calculates Latency
func (c *Heartbeat) ReadParams(r io.Reader) error {
	var size uint8
	err := binary.Read(r, binary.LittleEndian, &size)
	if err != nil {
		return err
	}

	c.T = make([]int64, size)
	return binary.Read(r, binary.LittleEndian, &c.T)
}

// WriteParams writes Page and PageSize to specified writer
func (c *Heartbeat) WriteParams(w io.Writer) error {
	err := binary.Write(w, binary.LittleEndian, uint8(len(c.T)))
	if err != nil {
		return err
	}
	return binary.Write(w, binary.LittleEndian, c.T)
}

// ServerCommand returns server command code
func (c *Heartbeat) ServerCommand() model.ServerCommand {
	return model.ServerCommandHeartbeat
}
