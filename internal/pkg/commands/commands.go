package commands

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"

	"bitbucket.org/s2323/messaging/internal/pkg/model"
)

// ServerCommand interface
type ServerCommand interface {
	ServerCommand() model.ServerCommand
	WriteParams(w io.Writer) error
	ReadParams(r io.Reader) error
}

// Write specified command into specified writer
func Write(w io.Writer, cmd ServerCommand) error {
	err := binary.Write(w, binary.LittleEndian, uint8(cmd.ServerCommand()))
	if err != nil {
		return err
	}
	return cmd.WriteParams(w)
}

// Read server command from specified reader
func Read(r io.Reader) (ServerCommand, error) {
	var code uint8
	err := binary.Read(r, binary.LittleEndian, &code)
	if err != nil {
		return nil, err
	}

	var cmd ServerCommand
	switch model.ServerCommand(code) {
	case model.ServerCommandList:
		cmd = &ListPeers{}
	case model.ServerCommandHeartbeat:
		cmd = &Heartbeat{}
	default:
		return nil, fmt.Errorf("unsupported server command: code = %d (%s)", model.ServerCommand(code), model.ServerCommand(code).String())
	}

	err = cmd.ReadParams(r)
	if err != nil {
		return nil, err
	}
	return cmd, nil
}

// WriteToMessage specified command into message
func WriteToMessage(msg *model.Message, cmd ServerCommand) error {
	buf := bytes.NewBuffer(msg.Raw)
	err := Write(buf, cmd)
	msg.WithPayload(buf.Bytes())
	return err
}

// ReadFromMessage server command from message
func ReadFromMessage(msg *model.Message) (ServerCommand, error) {
	buf := bytes.NewBuffer(msg.Raw)
	return Read(buf)
}
