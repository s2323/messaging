package commands

import (
	"encoding/binary"
	"io"

	"bitbucket.org/s2323/messaging/internal/pkg/model"
)

const defaultPageSize = 20

// ListPeers is server command to paginate over connected peers
type ListPeers struct {
	Page     int
	PageSize int
}

// ReadParams reads and sets Page and PageSize from specified reader
func (c *ListPeers) ReadParams(r io.Reader) error {
	var tmp uint32
	err := binary.Read(r, binary.LittleEndian, &tmp)
	if err != nil {
		return err
	}
	c.Page = int(tmp)
	if c.Page <= 0 {
		c.Page = 1
	}

	err = binary.Read(r, binary.LittleEndian, &tmp)
	if err != nil {
		return err
	}
	c.PageSize = int(tmp)
	if c.PageSize <= 0 {
		c.PageSize = defaultPageSize
	}
	return nil
}

// WriteParams writes Page and PageSize to specified writer
func (c *ListPeers) WriteParams(w io.Writer) error {
	err := binary.Write(w, binary.LittleEndian, uint32(c.Page))
	if err != nil {
		return err
	}
	return binary.Write(w, binary.LittleEndian, uint32(c.PageSize))
}

// ServerCommand returns server command code
func (c *ListPeers) ServerCommand() model.ServerCommand {
	return model.ServerCommandList
}
