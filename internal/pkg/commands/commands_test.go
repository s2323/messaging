package commands

import (
	"bytes"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCommands(t *testing.T) {
	var buffer bytes.Buffer
	var commands []ServerCommand

	t.Run("should be able to write list peers command", func(t *testing.T) {
		cmd := &ListPeers{
			Page:     rand.Intn(10),
			PageSize: rand.Intn(100),
		}
		commands = append(commands, cmd)

		err := Write(&buffer, cmd)

		require.NoError(t, err)
	})

	t.Run("should be able to write heartbeat command", func(t *testing.T) {
		cmd := &Heartbeat{
			T: []int64{1, 2, 3},
		}
		commands = append(commands, cmd)

		err := Write(&buffer, cmd)

		require.NoError(t, err)
	})

	t.Run("should be able to read list peers command", func(t *testing.T) {
		cmd, err := Read(&buffer)
		listPeers, ok := cmd.(*ListPeers)

		require.NoError(t, err)
		require.True(t, ok)
		require.Equal(t, commands[0], listPeers)
	})

	t.Run("should be able to read heartbeat command", func(t *testing.T) {
		cmd, err := Read(&buffer)
		heartbeat, ok := cmd.(*Heartbeat)

		require.NoError(t, err)
		require.True(t, ok)
		require.Equal(t, commands[1], heartbeat)
	})
}
