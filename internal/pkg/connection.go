package pkg

import (
	"context"
	"io"
	"net"
	"sync"

	"bitbucket.org/s2323/messaging/internal/pkg/model"
)

// Connection ...
type Connection struct {
	conn   net.Conn
	ctx    context.Context
	cancel context.CancelFunc
	once   sync.Once

	// Outgoing messages queue
	out chan *model.Message

	// Incoming messages queue
	in chan *model.Message

	// Errors channel
	errs chan error
}

// NewConnection creates new Connection
func NewConnection(ctx context.Context, conn net.Conn) *Connection {
	c := &Connection{
		conn: conn,
		in:   make(chan *model.Message, 1),
		out:  make(chan *model.Message, 8),
		errs: make(chan error, 2),
	}
	c.ctx, c.cancel = context.WithCancel(ctx)
	return c
}

// Close connection
func (c *Connection) Close() error {
	err := c.conn.Close()
	c.cancel()
	c.once.Do(func() {
		close(c.in)
		close(c.errs)
	})
	return err
}

// RecvMessages reads messages from connection socket into incoming queue
func (c *Connection) RecvMessages() error {
	for c.ctx.Err() == nil {
		msg := &model.Message{}
		_, err := msg.ReadFrom(c.conn)
		if err != nil {
			if err == io.EOF {
				break // client disconnected
			}
			if v, success := err.(*net.OpError); success && v.Temporary() {
				c.errs <- err
			}
		} else {
			c.in <- msg
		}
	}
	return c.Close()
}

// SendMessages writes messages from outgoing queue into connection socket
func (c *Connection) SendMessages() error {
	for {
		select {
		case <-c.ctx.Done():
			return c.Close()
		case msg, ok := <-c.out:
			if !ok {
				return c.Close()
			}
			_, err := msg.WriteTo(c.conn)
			if err != nil {
				if err == io.EOF {
					return c.Close() // client disconnected
				}
				if v, success := err.(*net.OpError); success && v.Temporary() {
					c.errs <- err
				}
			}
		}
	}
}

// In returns channel to read incoming messages
func (c *Connection) In() chan *model.Message {
	return c.in
}

// Out returns channel to write outgoing messages
func (c *Connection) Out() chan *model.Message {
	return c.out
}

// Errs returns channel to read errors
func (c *Connection) Errs() chan error {
	return c.errs
}
