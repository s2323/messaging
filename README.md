## Make

* Build binaries
```
make build
```

* Run tests
```
make test
```

* Build binaries and run tests
```
make
```

---

## Run

### Server
```
./bin/server
```
or with address specified
```
./bin/server --addr='127.0.0.1:12345'
```

### Client
```
./bin/client
```
or with server address specified
```
./bin/client --addr='127.0.0.1:12345'
```

---

## TODO:
- configuration
